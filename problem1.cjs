const fs = require('fs');
//const dirPath = './ir'

  exports.function1= function makeDir(dirPath){
     const makedir = new Promise( function( resolve, reject){
      fs.mkdir(dirPath, (err ) => {
        if (err) {
         reject( err);
        } else {
         // console.log(dirPath);
          resolve (dirPath);
        }
      });
     })
      return makedir;
  }

exports.function2=function cbRemoveDir(dirPath) {
  fs.rmdir(dirPath, (err) => {
    if (err) {
      console.log(err);
    } else {
      console.log(`deleted ${dirPath}`);
    }
  });
}
//  module.exports.makeDir= makeDir;
//   module.exports.cbRemoveDir= cbRemoveDir;