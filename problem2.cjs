const fs = require("fs");

exports.mainFunction = function mainFunction(filePath, filesStorePath) {
  return new Promise((resolve, reject) => {
    fs.readFile(filePath, "utf8", function (error, data) {
      if (error) {
        reject(error);
      } else {
        let dataAndFilePath = [];
        dataAndFilePath.push(data);
        dataAndFilePath.push(filesStorePath);
        resolve(dataAndFilePath);
      }
    });
  });
};
exports.UpperContent = function UpperContent(data) {
  return new Promise((resolve, reject) => {
    const Upperdata = data[0].toUpperCase();
    const UpperfilePath = "./Upper.txt";
     filesStorePath=data[1];
    fs.writeFile(UpperfilePath, Upperdata, function (err) {
      if (err) {
        reject(err);
      } else {
        fs.writeFile(filesStorePath, UpperfilePath + "\n", function (error) {
          if (error) {
            reject(error);
          } else {
            fs.readFile(UpperfilePath, "utf8", (error, data) => {
              if (error) {
                reject(error);
              } else {
                let dataAndFilePath = [];
                dataAndFilePath.push(data);
                dataAndFilePath.push(filesStorePath);
                resolve(dataAndFilePath);
              }
            });
          }
        });
      }
    });
  });
};
exports.lowerFile = function lowerFile(data, filesStorePath) {
  return new Promise((resolve, reject) => {
    const lowerFilePath = "./Lower.txt";
    const lowerData = data[0].toLowerCase();
    const sentences = lowerData.split(".");
    filesStorePath=data[1];

    const convertedData = sentences.join("\n");

    fs.writeFile(lowerFilePath, convertedData, function (err) {
      if (err) {
        reject(err);
      } else {
        fs.appendFile(filesStorePath, lowerFilePath + "\n", (error, data) => {
          if (error) {
            reject(error);
          } else {
            fs.readFile(lowerFilePath, "utf8", (error, data) => {
              if (error) {
                reject(error);
              } else {
                let dataAndFilePath = [];
                dataAndFilePath.push(data);
                dataAndFilePath.push(filesStorePath);
                resolve(dataAndFilePath);
              }
            });
          }
        });
      }
    });
  });
};

exports.sortedFile = function sortedFile(data, filesStorePath) {
  return new Promise((resolve, reject) => {
    const sortedFile = "./SortedFile.txt";
    const sortedContent = data[0].split("\n").sort().join("\n");
    filesStorePath=data[1];
    fs.writeFile(sortedFile, sortedContent, function (err) {
      if (err) {
        reject(err);
      } else {
        fs.appendFile(filesStorePath, sortedFile + "\n", (error, data) => {
          if (error) {
            reject(err);
          } else {
            fs.readFile(filesStorePath, "utf8", (error, data) => {
              if (error) {
                reject(error);
              } else {
                const filesNames = data.split("\n");
                resolve(filesNames);
              }
            });
          }
        });
      }
    });
  });
};

exports.deleteFiles = function deleteFiles(filesNames) {
  filesNames.forEach((filename) => {
    if (filename) {
      fs.unlink(filename.trim(), (err) => {
        if (err) {
          console.error("Error deleting file:", err);
          return;
        }
        console.log("File deleted:", filename);
      });
    }
  });
};
//module.exports = mainFunction;
/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/
