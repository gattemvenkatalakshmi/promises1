const { reject } = require("async");
const fs = require("fs");

exports.CreatingJSONFiles = function CreatingJSONFiles(dirPath) {
  return new Promise((resolve, reject) => {
    let Number = Math.floor(Math.random() * 10 + 1);
    for (let index = 0; index < Number; index++) {
      let randomFilePath = dirPath + "/file" + index + ".json";
      fs.writeFile(randomFilePath, " ", function (error) {
        if (error) {
          //console.log(error);
          reject(error);
        } else {
          console.log(`file${index} is created`);
          //deleteFiles(randomFilePath);
          fs.unlink(randomFilePath, (error) => {
            if (error) {
              reject(error);
            } else {
               console.log(randomFilePath+"is deleted");
              //resolve(randomFilePath);
            }
          });
        }
      });
    }
  });
};
exports.checkDirExists = function checkDirExists(dirPath) {
  return new Promise((resolve, reject) => {
    fs.stat(dirPath, (err, stats) => {
      if (err) {
        // console.error("Error checking directory:", err);
        // return;
        reject(err);
      }
      if (stats.isDirectory()) {
        //CreatingJSONFiles(dirPath);
        resolve(dirPath);
      } else {
        fs.mkdir(dirPath, (err) => {
          if (err) {
            reject(err);
          } else {
            //CreatingJSONFiles(dirPath);
            resolve(dirPath);
          }
        });
      }
    });
  });
};

/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/
