const problem2= require('../problem2.cjs')
const filePath = "../lipsum_1.txt";
const filesStorePath = "../filesnames.txt";
problem2.mainFunction(filePath,filesStorePath).then(function(data){
    return problem2.UpperContent(data);
}).then(function(data){
    return problem2.lowerFile(data);
})
.then(function(data){
    return problem2.sortedFile(data);
})
.then(function( filesNames){
    return problem2.deleteFiles(filesNames);
})
.catch((error)=>{
    console.log( error);
})